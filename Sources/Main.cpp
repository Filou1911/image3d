#ifndef MAIN_CPP
#define MAIN_CPP

#include <QApplication>

#include <iostream>
#include <string>

#include "Widgets/MainWindow.hpp"

int main( int argc, char** argv )
{
	QApplication app( argc, argv );
	app.setApplicationName( "Image3D" );

	MainWindow mainWindow;
	mainWindow.show();

	return app.exec();
}

#endif // MAIN_CPP //