#ifndef MODBRIGHTNESS_CPP
#define MODBRIGHTNESS_CPP

#include "ModBrightness.hpp"

ModBrightness::ModBrightness ( QWidget* ptrParent )
: Modifier( "Brightness", -255, 255, 0, ptrParent )
{}

void ModBrightness::apply ( Image3D& image ) const
{
	for ( UInt x( 0 ) ; x < image.width()  ; ++x )
	for ( UInt y( 0 ) ; y < image.depth()  ; ++y )
	for ( UInt z( 0 ) ; z < image.height() ; ++z )
	{
		if ( image.at( x, y, z ) != 0 )
		{
			int result( (int)image.at( x, y, z ) + this->value() );
			if ( result < 0   ) result = 0;
			if ( result > 255 ) result = 255;
			image.at( x, y, z ) = (UChar)result;
		}
	}
}


#endif // MODBRIGHTNESS_CPP