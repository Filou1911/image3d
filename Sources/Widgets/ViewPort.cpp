#ifndef VIEWPORT_CPP
#define VIEWPORT_CPP

#include "ViewPort.hpp"

ViewPort::ViewPort ( QWidget* ptrParent )
: QWidget( ptrParent ), m_ptrLayout( new QHBoxLayout( this ) ), m_ptrScene( new SceneGL( 60, this ) ), m_ptrSettings( new Settings( this ) )
{
	this->setLayout( this->ptrLayout() );

	// SETUP SCENE AND ADD IT
	this->ptrSettings()->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
	this->ptrScene()->setFocusPolicy( Qt::ClickFocus );
	this->ptrLayout()->addWidget( this->ptrScene(), 1 );

	// SETUP SETTINGS AND ADD IT
	this->ptrSettings()->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Maximum );
	connect( this->ptrSettings(), SIGNAL( applyChanges(void) ), this, SLOT( applySettings(void) ) );
	this->ptrLayout()->addWidget( this->ptrSettings(), 0, Qt::AlignVCenter );

	// CREATE THE SETTINGS
	this->ptrSettings()->addModifier( new ModContrast   ( this ) );
	this->ptrSettings()->addModifier( new ModBrightness ( this ) );
}

const QHBoxLayout*	ViewPort::ptrLayout		( void ) const	{ return this->m_ptrLayout;		}
	  QHBoxLayout*	ViewPort::ptrLayout		( void )		{ return this->m_ptrLayout;		}
const SceneGL*		ViewPort::ptrScene		( void ) const	{ return this->m_ptrScene;		}
	  SceneGL*		ViewPort::ptrScene		( void )		{ return this->m_ptrScene;		}
const Settings*		ViewPort::ptrSettings	( void ) const	{ return this->m_ptrSettings;	}
	  Settings*		ViewPort::ptrSettings	( void )		{ return this->m_ptrSettings;	}
const Image3D&		ViewPort::originalImage	( void ) const	{ return this->m_originalImage;	}
	  Image3D&		ViewPort::originalImage	( void )		{ return this->m_originalImage;	}
const Image3D&		ViewPort::image			( void ) const	{ return this->m_image;			}
	  Image3D&		ViewPort::image			( void ) 		{ return this->m_image;			}

void ViewPort::resetView ( void )
{
	this->ptrScene()->resetView();
}

void ViewPort::open( void )
{
	QString fileName = QFileDialog::getOpenFileName( this );
	if ( ! fileName.isEmpty() )
	{
		// Load the image
		this->originalImage() = Image3D( fileName.toStdString() );
		this->image()         = this->originalImage();

		// Set for display
		this->ptrScene()->ptrImage() = &( this->image() );
	}
}

void ViewPort::applySettings ( void )
{
	// Disable the display:
	this->ptrScene()->ptrImage() = NULL;

	// reload the original image
	this->image() = this->originalImage();

	// Apply each setting
	for ( const Modifier* ptrModifier : *this->ptrSettings() )
		ptrModifier->apply( this->image() );

	// Set for display
	this->ptrScene()->ptrImage() = &( this->image() );
}

#endif // VIEWPORT_HPP 
