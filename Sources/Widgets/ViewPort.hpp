#ifndef VIEWPORT_HPP
#define VIEWPORT_HPP

#include "SceneGL.hpp"
#include "Settings.hpp"

#include "ModBrightness.hpp"
#include "ModContrast.hpp"

class ViewPort : public QWidget
{
	Q_OBJECT
private:
	QHBoxLayout*	m_ptrLayout;
	SceneGL*		m_ptrScene;
	Settings*		m_ptrSettings;
	Image3D			m_originalImage;
	Image3D			m_image;

public:
	ViewPort ( QWidget* ptrParent );

	const QHBoxLayout*	ptrLayout		( void ) const;
		  QHBoxLayout*	ptrLayout		( void );
	const SceneGL*		ptrScene		( void ) const;
		  SceneGL*		ptrScene		( void );
	const Settings*		ptrSettings		( void ) const;
		  Settings*		ptrSettings		( void );
	const Image3D&		originalImage	( void ) const;
		  Image3D&		originalImage	( void );
	const Image3D&		image			( void ) const;
		  Image3D&		image			( void );

public slots:
	void resetView		( void );
	void open			( void );
	void applySettings	( void );
};

#endif // VIEWPORT_HPP 