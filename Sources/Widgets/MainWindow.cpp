#ifndef MAINWINDOW_CPP
#define MAINWINDOW_CPP

// DEPENDANCES ------------------------------------------------------------------------------------

#include "MainWindow.hpp"

// CONSTRUCTOR ------------------------------------------------------------------------------------

MainWindow::MainWindow ( QWidget* ptrParent )
: QMainWindow( ptrParent ), m_ptrViewPort( new ViewPort( this ) )
{
	this->resize( 800, 600 );
	this->setUnifiedTitleAndToolBarOnMac( true );
	
	this->setCentralWidget( this->ptrViewPort() );

	// File > Open ...................................................................
	QAction* openAct = new QAction( "Open...", this );
			 openAct->setShortcuts( QKeySequence::Open );
			 openAct->setStatusTip( "Open an existing scene" );
	connect( openAct, SIGNAL( triggered(void) ), this->ptrViewPort(), SLOT( open(void) ) );

	// File > Exit ...................................................................
	QAction* exitAct = new QAction( "Exit", this );
			 exitAct->setShortcuts( QKeySequence::Quit );
			 exitAct->setStatusTip( "Exit the application" );
	connect( exitAct, SIGNAL( triggered(void) ), this, SLOT( close(void) ) );

	// View > Reset ..................................................................
	QAction* resetViewAct = new QAction( "Reset", this );
			 resetViewAct->setStatusTip( "Reset the orientation, zoom, and panning of the 3D view" );
	connect( resetViewAct, SIGNAL( triggered(void ) ), this->ptrViewPort(), SLOT( resetView(void) ) );

	// Create Menus ------------------------------------------------------------------
	QMenu*	fileMenu = this->menuBar()->addMenu( "File" );
			fileMenu->addAction( openAct );
			fileMenu->addSeparator();
			fileMenu->addAction( exitAct );

	QMenu*	viewMenu = this->menuBar()->addMenu( "View" );
			viewMenu->addAction( resetViewAct );

	this->menuBar()->addSeparator();
}

// PRIVATE GETTERS / SETTERS ----------------------------------------------------------------------

const ViewPort*	MainWindow::ptrViewPort	( void ) const	{ return this->m_ptrViewPort;	}
ViewPort*		MainWindow::ptrViewPort	( void )		{ return this->m_ptrViewPort;	}

#endif // MAINWINDOW_CPP //