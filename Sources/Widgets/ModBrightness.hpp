#ifndef MODBRIGHTNESS_HPP
#define MODBRIGHTNESS_HPP

#include "Modifier.hpp"

class ModBrightness : public Modifier
{
	Q_OBJECT
public:
	ModBrightness ( QWidget* ptrParent = 0 );

	virtual void apply ( Image3D& image ) const;
};

#endif // MODBRIGHTNESS_HPP