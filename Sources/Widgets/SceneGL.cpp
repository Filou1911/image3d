#ifndef SCENEGL_CPP
#define SCENEGL_CPP

// Dependances : //

#include "SceneGL.hpp"

// #################################################################################################### //
// ###                                   CONSTRUCTOR / DESTRUCTOR                                   ### //
// #################################################################################################### //

SceneGL::SceneGL ( int framesPerSecond, QWidget* ptrParent )
: QGLWidget( ptrParent ), m_ptrTimer( new QTimer( this ) ), m_ptrImage( NULL ),
  lastPositionX( 0 ), lastPositionY( 0 ), angleX( 0 ), angleY( 0 ), zoomFactor( 1.0f )
{
	connect( m_ptrTimer, SIGNAL( timeout(void) ), this, SLOT( tick(void) ) );
	m_ptrTimer->start( 1000.0 / framesPerSecond );
}

// #################################################################################################### //
// ###                                       GETTER / SETTER                                        ### //
// #################################################################################################### //

const Image3D*	SceneGL::ptrImage 	( void ) const 	{ return this->m_ptrImage;		}
const Image3D*&	SceneGL::ptrImage 	( void )	 	{ return this->m_ptrImage;		}

void SceneGL::clear ( void )
{
	this->ptrImage() = NULL;
}

// #################################################################################################### //
// ###                                            SLOTS                                             ### //
// #################################################################################################### //

void SceneGL::tick ( void )
{
	this->updateGL();
}

void SceneGL::resetView ( void )
{
	// View angle
	this->angleX = 0.0f;
	this->angleY = 0.0f;

	// View zoom
	this->zoomFactor = 1.0f;
}


void SceneGL::rotate ( float onX, float onY )
{
	this->angleX += onX;
	this->angleY += onY;
}

void SceneGL::zoom ( float factor )
{
	this->zoomFactor += factor;

	if ( this->zoomFactor < 1.0f )
		this->zoomFactor = 1;

	if ( this->zoomFactor > 400.0f )
		this->zoomFactor = 400.0f;
}

// #################################################################################################### //
// ###                                        EVEN RECEPTION                                        ### //
// #################################################################################################### //

void SceneGL::mousePressEvent ( QMouseEvent* event )
{
	this->lastPositionX = event->localPos().x();
	this->lastPositionY = event->localPos().y();
	event->accept();
}

void SceneGL::mouseMoveEvent ( QMouseEvent* event )
{
	if ( event->button() == Qt::NoButton )
	{
		this->rotate(
			this->lastPositionX - event->localPos().x(),
			this->lastPositionY - event->localPos().y()
		);

		this->lastPositionX = event->localPos().x();
		this->lastPositionY = event->localPos().y();

		event->accept();
	}
	else
		event->ignore();
}

void SceneGL::mouseReleaseEvent ( QMouseEvent* event )
{
	this->lastPositionX = event->localPos().x();
	this->lastPositionY = event->localPos().y();
	event->accept();
}

void SceneGL::keyPressEvent	( QKeyEvent* event )
{
	switch( event->key() )
	{
		case Qt::Key_Plus:
		case Qt::Key_Equal:
			this->zoom( 0.5f );
			break;

		case Qt::Key_Minus:
		case Qt::Key_Underscore:
			this->zoom( -0.5f );
			break;

		case Qt::Key_R:
			this->resetView();
			break;

		default:
			event->ignore();
			return;
	}
	event->accept();
}

void SceneGL::keyReleaseEvent ( QKeyEvent* event )
{
	event->accept();
}


// #################################################################################################### //
// ###                                            OPENGL                                            ### //
// #################################################################################################### //

void
SceneGL::initializeGL ( void )
{
	glShadeModel( GL_SMOOTH );
	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
	glClearDepth( 1.0f );
	glEnable( GL_DEPTH_TEST );
	glDepthFunc( GL_LEQUAL );
	glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
	glBlendEquation( GL_FUNC_ADD );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glEnable( GL_BLEND );
}

void
SceneGL::paintGL ( void )
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glLoadIdentity();
	if ( this->ptrImage() != NULL )
		glTranslatef( 0.0f, 0.0f, -15-this->ptrImage()->diagonal() );
	else
		glTranslatef( 0.0f, 0.0f, -30.0f );
	glRotatef( 45.0+this->angleX, 0.0, 1.0, 0.0 );
	glRotatef( 30.0+this->angleY, 1.0, 0.0, 1.0 );
	glScalef( 1.0f*this->zoomFactor, 1.0f*this->zoomFactor, 1.0f*this->zoomFactor );

	if ( this->ptrImage() != NULL )
	{
		GLDrawer drawer;
		drawer << *this->ptrImage() << GLDrawer::endDraw;
	}
	else
		SceneGL::drawGrid();

}

void
SceneGL::resizeGL( int width, int height )
{
	if( height == 0 )
		height = 1;
	glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	gluPerspective( 45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 1000.0f );
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
}

void
SceneGL::drawGrid ( void )
{
	glBegin( GL_LINES );

	// draw the lines
	for ( int x( -5 ) ; x < 5 ; ++x )
	for ( int z( -5 ) ; z < 5 ; ++z )
	{
		glColor4f( 0.7f, 0.7f, 0.7f, 1.0f );

		if ( x == -5 )
		{
			glVertex3f( x, 0.0f, z );
			glVertex3f( x, 0.0f, z+1 );
		}
		if ( z == -5 )
		{
			glVertex3f( x,      0.0f, z );
			glVertex3f( x+1.0f, 0.0f, z );
		}
		
		if ( x == -5 && z == -5 )
			glColor4f( 0.0f, 0.7f, 0.0f, 1.0f );

		glVertex3f( x, 0, z   );
		glVertex3f( x, 0, z+1 );

		if ( x == -5 && z == -5 )
			glColor4f( 0.7f, 0.0f, 0.0f, 1.0f );

		glVertex3f( x,   0, z );
		glVertex3f( x+1, 0, z );

		if ( x == 4 )
		{
			glVertex3f( x+1, 0.0f, z );
			glVertex3f( x+1, 0.0f, z+1 );
		}
		if ( z == 4 )
		{
			glVertex3f( x,      0.0f, z+1 );
			glVertex3f( x+1.0f, 0.0f, z+1 );
		}
	}

	glEnd();
}

#endif // SCENEGL_CPP //
