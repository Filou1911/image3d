#ifndef SCENEGL_HPP
#define SCENEGL_HPP

#include "Containers/Image3D.hpp"
#include "GLDrawer.hpp"

class SceneGL : public QGLWidget
{
	Q_OBJECT
private:
	// MEMBERS : //
	QTimer*			m_ptrTimer;
	const Image3D*	m_ptrImage;

	int m_minThreshold, m_maxThreshold;

	// Mouse informations:
	int lastPositionX, lastPositionY;
	float angleX, angleY, zoomFactor;

public:
	// CONSTRUCTOR : //
	SceneGL ( int framesPerSecond = 60, QWidget* parent = 0 );

	// GETTERS - SETTERES : //
	const Image3D*	ptrImage		( void ) const;
	const Image3D*&	ptrImage		( void );
	void			clear			( void );

	// EVENTS : //
protected:
	virtual void	mousePressEvent		( QMouseEvent*	event ) Q_DECL_OVERRIDE;
	virtual void	mouseMoveEvent		( QMouseEvent*	event ) Q_DECL_OVERRIDE;
	virtual void	mouseReleaseEvent	( QMouseEvent*	event ) Q_DECL_OVERRIDE;
	virtual void	keyPressEvent		( QKeyEvent*	event ) Q_DECL_OVERRIDE;
	virtual void	keyReleaseEvent		( QKeyEvent*	event ) Q_DECL_OVERRIDE;

	// OPENGL : //
	virtual void initializeGL	( void ) Q_DECL_OVERRIDE;
	virtual void paintGL		( void ) Q_DECL_OVERRIDE;
	virtual void resizeGL		( int width, int height ) Q_DECL_OVERRIDE;
		
	// SLOTS : //
public slots:
	void tick       ( void );
	void resetView  ( void );
	void rotate		( float onX, float onY );
	void zoom		( float factor );

	// STATIC
	static void drawGrid ( void );

};

#endif // SCENEGL_HPP //
