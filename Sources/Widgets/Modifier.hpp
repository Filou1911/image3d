#ifndef MODIFIER_HPP
#define MODIFIER_HPP

#include <QtWidgets>
#include "Containers/Image3D.hpp"

class Modifier : public QWidget
{
	Q_OBJECT
private:
	QHBoxLayout*	m_ptrLayout;
	QLabel*			m_ptrLabel;
	QSpinBox*		m_ptrSpinBox;

public:
	Modifier ( const QString& text, int min = 0, int max = 255, int value = 0, QWidget* ptrParent = NULL );

private:
	virtual const QHBoxLayout*	ptrLayout	( void ) const	final;
	virtual QHBoxLayout* 		ptrLayout	( void )		final;
	virtual const QLabel*		ptrLabel	( void ) const	final;
	virtual QLabel* 			ptrLabel	( void )		final;
	virtual const QSpinBox*		ptrSpinBox	( void ) const	final;
	virtual QSpinBox* 			ptrSpinBox	( void )		final;

public:
	virtual QString	text	( void ) const final;
	virtual int		minimum	( void ) const final;
	virtual int		maximum	( void ) const final;
	virtual int		value	( void ) const final;

	virtual void	setText		( const QString& text ) final;
	virtual void	setMinimum	( int min ) final;
	virtual void	setMaximum	( int max ) final;
	virtual void	setValue	( int value ) final;

	virtual void apply ( Image3D& image ) const = 0;

signals:
	void valueChanged ( int value );

private slots:
	void slotValueChanged ( void );

};

#endif // MODIFIER_HPP //