#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include "ViewPort.hpp"

class MainWindow : public QMainWindow
{
	Q_OBJECT
private:
	ViewPort* m_ptrViewPort;

	const ViewPort* ptrViewPort ( void ) const;
		  ViewPort* ptrViewPort ( void );

public:
	MainWindow ( QWidget* ptrParent = 0 );
};

#endif // MAINWINDOW_HPP //