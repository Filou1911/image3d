#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <QtWidgets>
#include "Dependances.hpp"
#include "Modifier.hpp"

class Settings : public QWidget, private vector< Modifier* >
{
	Q_OBJECT
private:
	QVBoxLayout*	m_ptrLayout;
	QLabel*			m_ptrLabel;
	QPushButton*	m_ptrButton;

public:
	Settings ( QWidget* parent = NULL );

private:
	const QVBoxLayout*	ptrLayout	( void ) const;
	QVBoxLayout*		ptrLayout	( void );
	const QLabel*		ptrLabel	( void ) const;
	QLabel*				ptrLabel	( void );
	const QPushButton*	ptrButton	( void ) const;
	QPushButton*		ptrButton	( void );

public:
	using vector< Modifier* >::size;
	using vector< Modifier* >::at;
	using vector< Modifier* >::operator[];
	using vector< Modifier* >::begin;
	using vector< Modifier* >::end;
	using vector< Modifier* >::cbegin;
	using vector< Modifier* >::cend;

	void addModifier ( Modifier* ptrModifier );

signals:
	void applyChanges ( void );

private slots:
	void buttonPushed ( void );
};

#endif // SETTINGS_HPP //