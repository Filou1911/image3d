#ifndef GLDRAWER_CPP
#define GLDRAWER_CPP

#include "GLDrawer.hpp"

GLDrawer::GLDrawer ( void )
{
	glBegin( GL_QUADS );
}

int GLDrawer::endDraw( 666 );

GLDrawer& GLDrawer::operator<< ( int flag )
{
	if ( flag == GLDrawer::endDraw )
		glEnd();

	return (*this);
}

GLDrawer& GLDrawer::operator<< ( const Image3D& image )
{
	UInt  width( image.width()  );
	UInt  depth( image.depth()  );
	UInt height( image.height() );

	float  width_2(  (float)width / 2.0f - 0.5f );
	float  depth_2(  (float)depth / 2.0f - 0.5f );
	float height_2( (float)height / 2.0f - 0.5f );

	glBegin( GL_QUADS );

	for ( UInt x( 0 ) ; x < width  ; ++x )
	for ( UInt y( 0 ) ; y < depth  ; ++y )
	for ( UInt z( 0 ) ; z < height ; ++z )
	{
		UChar value( image.at( x, y, z ) );

		if ( value != 0 )
		{
			// Center of mass of the voxel to draw:
			float pos_x( x-width_2  );
			float pos_y( y-depth_2  );
			float pos_z( z-height_2 );

			// Check wich face we will draw:
			int facelist( CubeFace::Type::None );

			if ( x == 0 || image.at( x-1, y, z ) < value )
				facelist |= CubeFace::Type::Left;

			if ( x == width-1 || image.at( x+1, y, z ) < value )
				facelist |= CubeFace::Type::Right;

			if ( y == 0 || image.at( x, y-1, z ) < value )
				facelist |= CubeFace::Type::Back;
			
			if ( y == depth-1 || image.at( x, y+1, z ) < value )
				facelist |= CubeFace::Type::Front;

			if ( z == 0 || image.at( x, y, z-1 ) < value )
				facelist |= CubeFace::Type::Bottom;

			if ( z == height-1 || image.at( x, y, z+1 ) < value )
				facelist |= CubeFace::Type::Top;

			// Create and draw the cube:
			float color( (float)value / 255.0f );
			(*this) << Cube( pos_x, pos_z, pos_y, Color( color, color ), facelist );
		}
	}
	return (*this);
}

GLDrawer& GLDrawer::operator<< ( const Vertex& vertex )
{
	glVertex3f( vertex.x, vertex.y, vertex.z );
	return (*this);
}

GLDrawer& GLDrawer::operator<< ( const vector< Vertex >& vertices )
{
	for ( const Vertex& vertex : vertices )
		(*this) << vertex;

	return (*this);
}

GLDrawer& GLDrawer::operator<< ( const Color& color )
{
	glColor4f( color.r, color.g, color.b, color.a );
	return (*this);
}

GLDrawer& GLDrawer::operator<< ( const Normal& normal )
{
	glNormal3f( normal.dx, normal.dy, normal.dz );
	return (*this);
}

GLDrawer& GLDrawer::operator<< ( const CubeFace& cubeFace )
{
	(*this) << cubeFace.vertices;
	(*this) << cubeFace.normal;
	return (*this);
}

GLDrawer& GLDrawer::operator<< ( const Cube& cube )
{
	(*this) << cube.color;

	for ( const CubeFace& face : cube.faces )
		(*this) << face;

	return (*this);
}

#endif // GLDRAWER_CPP