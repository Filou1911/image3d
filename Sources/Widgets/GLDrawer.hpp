#ifndef GLDRAWER_HPP
#define GLDRAWER_HPP

#include "Containers/Cube.hpp"
#include "Containers/Image3D.hpp"

class GLDrawer
{
public:
	GLDrawer ( void );

	static int endDraw;

	GLDrawer& operator<< ( int flag );
	GLDrawer& operator<< ( const Vertex& vertex );
	GLDrawer& operator<< ( const vector< Vertex >& vertices );
	GLDrawer& operator<< ( const Color& color );
	GLDrawer& operator<< ( const Normal& normal );
	GLDrawer& operator<< ( const CubeFace& cubeFace );
	GLDrawer& operator<< ( const Cube& cube );
	GLDrawer& operator<< ( const Image3D& image );
};

#endif // GLDRAWER_HPP