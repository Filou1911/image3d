#ifndef MODCONTRAST_CPP
#define MODCONTRAST_CPP

#include "ModContrast.hpp"

ModContrast::ModContrast ( QWidget* ptrParent )
: Modifier( "Contrast", 0, 255, 0, ptrParent )
{}

void ModContrast::apply ( Image3D& image ) const
{
	for ( UInt x( 0 ) ; x < image.width()  ; ++x )
	for ( UInt y( 0 ) ; y < image.depth()  ; ++y )
	for ( UInt z( 0 ) ; z < image.height() ; ++z )
	{
		if ( image.at( x, y, z ) != 0 )
		{
			int result( image.at( x, y, z ) );

			if ( result > 51 )
				result -= this->value();
			else
				result += this->value();

			if ( result < 0   ) result = 0;
			if ( result > 255 ) result = 255;

			image.at( x, y, z ) = (UChar)result;
		}
	}
}

#endif // MODCONTRAST_CPP