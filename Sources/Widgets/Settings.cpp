#ifndef SETTINGS_CPP
#define SETTINGS_CPP

// DEPENDANCES ------------------------------------------------------------------------------------

#include "Settings.hpp"

// CONSTRUCTOR ------------------------------------------------------------------------------------

Settings::Settings ( QWidget* parent )
: m_ptrLayout( new QVBoxLayout( this ) ), m_ptrLabel( new QLabel( "Settings:", this ) ), m_ptrButton( new QPushButton( "Apply", this ) )
{
	this->ptrLabel()->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Maximum );
	this->ptrLayout()->addWidget( this->ptrLabel(), 1, Qt::AlignHCenter | Qt::AlignTop );

	this->ptrButton()->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum );
	this->ptrLayout()->addWidget( this->ptrButton(), 0, Qt::AlignHCenter | Qt::AlignBottom );
	connect( this->ptrButton(), SIGNAL( released() ), this, SLOT( buttonPushed(void) ) );
}

// PRIVATE GETTERS / SETTERS ----------------------------------------------------------------------

const QVBoxLayout*	Settings::ptrLayout	( void ) const	{ return m_ptrLayout;	}
QVBoxLayout*		Settings::ptrLayout	( void )		{ return m_ptrLayout;	}
const QLabel*		Settings::ptrLabel	( void ) const	{ return m_ptrLabel;	}
QLabel*				Settings::ptrLabel	( void )		{ return m_ptrLabel;	}
const QPushButton*	Settings::ptrButton	( void ) const	{ return m_ptrButton;	}
QPushButton*		Settings::ptrButton	( void )		{ return m_ptrButton;	}

// PUBLIC GETTERS / SETTERS -----------------------------------------------------------------------

void Settings::addModifier ( Modifier* ptrModifier )
{
	ptrModifier->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum );
	this->ptrLayout()->insertWidget( (this->size()+1), ptrModifier, 1, Qt::AlignHCenter | Qt::AlignTop );
	this->push_back( ptrModifier );
}

// PRIVATE SLOTS ----------------------------------------------------------------------------------

void Settings::buttonPushed ( void )
{
	emit this->applyChanges();
}

#endif // SETTINGS_CPP //