#ifndef MODCONTRAST_HPP
#define MODCONTRAST_HPP

#include "Modifier.hpp"

class ModContrast : public Modifier
{
public:
	ModContrast ( QWidget* ptrParent = NULL );

	virtual void apply ( Image3D& image ) const;
};

#endif // MODCONTRAST_HPP