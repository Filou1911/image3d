#ifndef NAMEDSLIDER_CPP
#define NAMEDSLIDER_CPP

// DEPENDANCES ------------------------------------------------------------------------------------

#include "Modifier.hpp"

// CONSTRUCTOR ------------------------------------------------------------------------------------

Modifier::Modifier( const QString& text, int min, int max, int value, QWidget* ptrParent )
: QWidget( ptrParent ), m_ptrLayout( new QHBoxLayout( this ) ), m_ptrLabel( new QLabel( text, this ) ), m_ptrSpinBox( new QSpinBox( this ) )
{
	// Label
	this->ptrLabel()->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum );
	this->ptrLayout()->addWidget( this->ptrLabel(), 0, Qt::AlignVCenter | Qt::AlignRight );

	// Slider
	this->ptrSpinBox()->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum );
	this->setMinimum( min );
	this->setMaximum( max );
	this->setValue( value );
	this->ptrLayout()->addWidget( this->ptrSpinBox(), 1, Qt::AlignVCenter | Qt::AlignLeft );

	connect( this->ptrSpinBox(), SIGNAL( valueChanged(int) ), this, SLOT( slotValueChanged(void) ) );
}

// PRIVATE GETTERS / SETTERS ----------------------------------------------------------------------

const QHBoxLayout*	Modifier::ptrLayout		( void ) const	{ return this->m_ptrLayout;		}
QHBoxLayout* 		Modifier::ptrLayout		( void )		{ return this->m_ptrLayout;		}
const QLabel*		Modifier::ptrLabel		( void ) const	{ return this->m_ptrLabel;		}
QLabel* 			Modifier::ptrLabel		( void )		{ return this->m_ptrLabel;		}
const QSpinBox*		Modifier::ptrSpinBox	( void ) const	{ return this->m_ptrSpinBox;	}
QSpinBox* 			Modifier::ptrSpinBox	( void )		{ return this->m_ptrSpinBox;	}

// PUBLIC GETTERS / SETTERS -----------------------------------------------------------------------

QString	Modifier::text		( void ) const	{ return this->ptrLabel()->text();		}
int		Modifier::minimum	( void ) const	{ return this->ptrSpinBox()->minimum();	}
int		Modifier::maximum	( void ) const	{ return this->ptrSpinBox()->maximum();	}
int		Modifier::value		( void ) const	{ return this->ptrSpinBox()->value();	}

void	Modifier::setText		( const QString& text )	{ this->ptrLabel()->setText( text );		}
void	Modifier::setMinimum	( int min )				{ this->ptrSpinBox()->setMinimum( min );	}
void	Modifier::setMaximum	( int max )				{ this->ptrSpinBox()->setMaximum( max );	}
void	Modifier::setValue		( int value )			{ this->ptrSpinBox()->setValue( value );	}

// PRIVATE SLOTS ----------------------------------------------------------------------------------

void Modifier::slotValueChanged ( void )
{
	emit valueChanged( this->value() );
}

#endif // NAMEDSLIDER_CPP //