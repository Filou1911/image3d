#ifndef CHUNK_CPP
#define CHUNK_CPP

#include "Image3D.hpp"

#define SQUARE( X ) ((X)*(X))

Image3D::Image3D ( UInt width, UInt depth, UInt height, UChar value )
: m_width( width ), m_depth( depth ), m_height( height ), m_data( width*depth*height, value )
{}

Image3D::Image3D ( const string& filename )
: m_width( 0 ), m_depth( 0 ), m_height( 0 )
{
	// Open the file : //
	ifstream inputFile( filename, ifstream::in );

	// Get the magic number : //
	string magicNumber;
	inputFile >> magicNumber;

	// Get the dimentions:
	inputFile >> this->width();
	inputFile >> this->depth();
	inputFile >> this->height();

	UInt totalSize( this->width() * this->depth() * this->height() );

	UInt maxValue( 0 );
	inputFile >> maxValue;

	// Allocate data:
	this->data() = vector< UChar >( totalSize, 0 );

	// Load data voxel by voxel:
	UInt index( 0 );
	while ( index < totalSize && inputFile.good() )
	{
		UInt value( 0 );
		inputFile >> value;
		this->data().at( index++ ) = (UChar)value;
	}

	// Close file:
	inputFile.close();
}

UInt	Image3D::shiftX	( UInt x ) const					{ return x;										}
UInt	Image3D::shiftY	( UInt y ) const					{ return y * this->width();						}
UInt	Image3D::shiftZ	( UInt z ) const					{ return z * this->width() * this->depth();		}
UInt	Image3D::shift	( UInt x, UInt y, UInt z ) const	{ return ( this->shiftX( x ) + this->shiftY( y ) + this->shiftZ( z ) );	}
UInt	Image3D::width	( void ) const						{ return this->m_width;		}
UInt&	Image3D::width	( void )							{ return this->m_width;		}
UInt	Image3D::depth	( void ) const						{ return this->m_depth;		}
UInt&	Image3D::depth	( void )							{ return this->m_depth;		}
UInt	Image3D::height	( void ) const						{ return this->m_height;	}
UInt&	Image3D::height	( void )							{ return this->m_height;	}

bool Image3D::isEmpty ( void ) const { return this->data().empty(); }

float Image3D::diagonal ( void ) const
{
	return this->width()/2 + this->depth()/2 + this->height()/2;
}

const vector< UChar >& Image3D::data ( void ) const	{ return this->m_data;	}
	  vector< UChar >& Image3D::data ( void )		{ return this->m_data;	}

UChar	Image3D::at ( UInt x, UInt y, UInt z ) const { return this->data().at( this->shift( x, y, z ) );	}
UChar&	Image3D::at ( UInt x, UInt y, UInt z )		 { return this->data().at( this->shift( x, y, z ) );	}

UChar	Image3D::operator ()( UInt x, UInt y, UInt z ) const { return this->at( x, y, z ); }
UChar&	Image3D::operator ()( UInt x, UInt y, UInt z )		 { return this->at( x, y, z ); }

Image3D&  Image3D::operator= ( const Image3D&  image )
{
	this->width()  = image.width();
	this->depth()  = image.depth();
	this->height() = image.height();
	this->data()   = image.data();
	return (*this);
}

#endif // CHUNK_CPP //