#ifndef CUBEFACE_HPP
#define CUBEFACE_HPP

#include "Vertex.hpp"
#include "Normal.hpp"

#include "Dependances.hpp"

class CubeFace
{
public:
	enum Type
	{
		None = 0, Front = 1, Back = 2, Left = 4, Right = 8, Top = 16, Bottom = 32
	};

	vector< Vertex > vertices;
	Normal normal;

	CubeFace ( vector< Vertex > vertices, const Normal& normal );
	CubeFace ( const CubeFace& cubeFace );
	CubeFace ( Type type, float center_x, float center_y, float center_z );

	static vector< Vertex > typeToVertices	( Type type, float center_x, float center_y, float center_z );
	static Normal			typeToNormal	( Type type );
};

#endif // CUBEFACE_HPP