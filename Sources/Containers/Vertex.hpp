#ifndef VERTEX_HPP
#define VERTEX_HPP

class Vertex
{
public:
	float x, y, z;
	Vertex ( float x = 0.0f, float y = 0.0f, float z = 0.0f );
	Vertex ( const Vertex& vertex );

	Vertex& set( float x, float y, float z );
};

#endif // VERTEX_HPP