#ifndef NORMAL_CPP
#define NORMAL_CPP

#include "Normal.hpp"

Normal::Normal ( float dx, float dy, float dz )
: dx( dx ), dy( dy ), dz( dz )
{}

Normal::Normal ( const Normal& normal )
: dx( normal.dx ), dy( normal.dy ), dz( normal.dz )
{}

Normal Normal::Zero   (  0,  0,  0 );
Normal Normal::Front  (  1,  0,  0 );
Normal Normal::Back   ( -1,  0,  0 );
Normal Normal::Top    (  0,  1,  0 );
Normal Normal::Bottom (  0, -1,  0 );
Normal Normal::Right  (  0,  0,  1 );
Normal Normal::Left   (  0,  0, -1 );

#endif // NORMAL_CPP