#ifndef CUBEFACE_CPP
#define CUBEFACE_CPP

#include "CubeFace.hpp"

CubeFace::CubeFace ( vector< Vertex > vertices, const Normal& normal )
: vertices( vertices ), normal( normal )
{}

CubeFace::CubeFace ( const CubeFace& cubeFace )
: vertices( cubeFace.vertices ), normal( cubeFace.normal )
{}

CubeFace::CubeFace ( Type type, float center_x, float center_y, float center_z )
: vertices( CubeFace::typeToVertices( type, center_x, center_y, center_z ) ), normal( CubeFace::typeToNormal( type ) )
{}

vector< Vertex > CubeFace::typeToVertices	( CubeFace::Type type, float center_x, float center_y, float center_z )
{
	vector< Vertex > vertices( 4 );
	switch ( type )
	{
		case CubeFace::Type::Front:
			vertices.at( 0 ).set( -0.5f+center_x, -0.5f+center_y,  0.5f+center_z );
			vertices.at( 1 ).set(  0.5f+center_x, -0.5f+center_y,  0.5f+center_z );
			vertices.at( 2 ).set(  0.5f+center_x,  0.5f+center_y,  0.5f+center_z );
			vertices.at( 3 ).set( -0.5f+center_x,  0.5f+center_y,  0.5f+center_z );
			break;
		case CubeFace::Type::Back:
			vertices.at( 0 ).set( -0.5f+center_x, -0.5f+center_y, -0.5f+center_z );
			vertices.at( 1 ).set( -0.5f+center_x,  0.5f+center_y, -0.5f+center_z );
			vertices.at( 2 ).set(  0.5f+center_x,  0.5f+center_y, -0.5f+center_z );
			vertices.at( 3 ).set(  0.5f+center_x, -0.5f+center_y, -0.5f+center_z );
			break;
		case CubeFace::Type::Top:
			vertices.at( 0 ).set( -0.5f+center_x,  0.5f+center_y, -0.5f+center_z );
			vertices.at( 1 ).set( -0.5f+center_x,  0.5f+center_y,  0.5f+center_z );
			vertices.at( 2 ).set(  0.5f+center_x,  0.5f+center_y,  0.5f+center_z );
			vertices.at( 3 ).set(  0.5f+center_x,  0.5f+center_y, -0.5f+center_z );
			break;
		case CubeFace::Type::Bottom:
			vertices.at( 0 ).set( -0.5f+center_x, -0.5f+center_y, -0.5f+center_z );
			vertices.at( 1 ).set(  0.5f+center_x, -0.5f+center_y, -0.5f+center_z );
			vertices.at( 2 ).set(  0.5f+center_x, -0.5f+center_y,  0.5f+center_z );
			vertices.at( 3 ).set( -0.5f+center_x, -0.5f+center_y,  0.5f+center_z );
			break;
		case CubeFace::Type::Right:
			vertices.at( 0 ).set(  0.5f+center_x, -0.5f+center_y, -0.5f+center_z );
			vertices.at( 1 ).set(  0.5f+center_x,  0.5f+center_y, -0.5f+center_z );
			vertices.at( 2 ).set(  0.5f+center_x,  0.5f+center_y,  0.5f+center_z );
			vertices.at( 3 ).set(  0.5f+center_x, -0.5f+center_y,  0.5f+center_z );
			break;
		case CubeFace::Type::Left:
			vertices.at( 0 ).set( -0.5f+center_x, -0.5f+center_y, -0.5f+center_z );
			vertices.at( 1 ).set( -0.5f+center_x, -0.5f+center_y,  0.5f+center_z );
			vertices.at( 2 ).set( -0.5f+center_x,  0.5f+center_y,  0.5f+center_z );
			vertices.at( 3 ).set( -0.5f+center_x,  0.5f+center_y, -0.5f+center_z );
			break;
		default:
			break;
	}
	return vertices;
}

Normal CubeFace::typeToNormal ( CubeFace::Type type )
{
	switch ( type )
	{
		case CubeFace::Type::Front:
			return Normal::Front;
		case CubeFace::Type::Back:
			return Normal::Back;
		case CubeFace::Type::Top:
			return Normal::Top;
		case CubeFace::Type::Bottom:
			return Normal::Bottom;
		case CubeFace::Type::Right:
			return Normal::Right;
		case CubeFace::Type::Left:
			return Normal::Left;
		default:
			return Normal::Zero;
	}
}

#endif // CUBEFACE_CPP