#ifndef VERTEX_CPP
#define VERTEX_CPP

#include "Vertex.hpp"

Vertex::Vertex ( float x, float y, float z )
: x( x ), y( y ), z( z )
{}

Vertex::Vertex ( const Vertex& vertex )
: x( vertex.x ), y( vertex.y ), z( vertex.z )
{}

Vertex& Vertex::set( float x, float y, float z )
{
	this->x = x;
	this->y = y;
	this->z = z;
	return (*this);
}

#endif // VERTEX_CPP