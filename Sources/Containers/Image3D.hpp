#ifndef IMAGE3D_HPP
#define IMAGE3D_HPP

#include "Dependances.hpp"

class Image3D
{
private:
	UInt m_width, m_depth, m_height;
	vector< UChar > m_data;

public:
	Image3D ( UInt width = 0, UInt depth = 0, UInt height = 0, UChar value = 0 );
	Image3D ( const string& filename );

	UInt	shiftX	( UInt x ) const;
	UInt	shiftY	( UInt y ) const;
	UInt	shiftZ	( UInt z ) const;
	UInt	shift	( UInt x, UInt y, UInt z ) const;

	UInt	width	( void ) const;
	UInt&	width	( void );
	UInt	depth	( void ) const;
	UInt&	depth	( void );
	UInt	height	( void ) const;
	UInt&	height	( void );

	bool	isEmpty	( void ) const;

	float	diagonal	( void ) const;

	const vector< UChar >& data ( void ) const;
		  vector< UChar >& data ( void );

	UChar	at ( UInt x, UInt y, UInt z ) const;
	UChar&	at ( UInt x, UInt y, UInt z );

	UChar	operator ()( UInt x, UInt y, UInt z ) const;
	UChar&	operator ()( UInt x, UInt y, UInt z );

	Image3D&  operator= ( const Image3D&  image );
};

#endif // IMAGE3D_HPP //