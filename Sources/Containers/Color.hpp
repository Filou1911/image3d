#ifndef COLOR_HPP
#define COLOR_HPP

class Color
{
public:
	float r, g, b, a;
	Color ( float gray = 0.0f, float alpha = 1.0f );
	Color ( float r, float g, float b, float a = 1.0f );
	Color ( const Color& color );
};

#endif // COLOR_HPP