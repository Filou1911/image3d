#ifndef COLOR_CPP
#define COLOR_CPP

#include "Color.hpp"

Color::Color ( float gray, float alpha )
: r( gray ), g( gray ), b( gray ), a( alpha )
{}

Color::Color ( float r, float g, float b, float a )
: r( r ), g( g ), b( b ), a( a )
{}

Color::Color ( const Color& color )
: r( color.r ), g( color.g ), b( color.b ), a( color.a )
{}

#endif // COLOR_CPP