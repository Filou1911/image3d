#ifndef NORMAL_HPP
#define NORMAL_HPP

class Normal
{
public:
	float dx, dy, dz;
	Normal ( float dx = 0.0f, float dy = 0.0f, float dz = 0.0f );
	Normal ( const Normal& normal );

	static Normal Zero;
	static Normal Front;
	static Normal Back;
	static Normal Top;
	static Normal Bottom;
	static Normal Right;
	static Normal Left;
};

#endif // NORMAL_HPP