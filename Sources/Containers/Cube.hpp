#ifndef CUBE_HPP
#define CUBE_HPP

#include "CubeFace.hpp"
#include "Color.hpp"

class Cube
{
public:
	Color color;
	vector< CubeFace > faces;

	Cube ( float center_x, float center_y, float center_z, Color color = Color(), int faces = CubeFace::Type::None );
	Cube ( const Cube& cube );

};

#endif // CUBE_HPP