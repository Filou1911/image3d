#ifndef CUBE_CPP
#define CUBE_CPP

#include "Cube.hpp"

Cube::Cube ( float center_x, float center_y, float center_z, Color color, int faces )
: color ( color )
{
	if ( faces & CubeFace::Type::Front )
		this->faces.push_back( CubeFace( CubeFace::Type::Front, center_x, center_y, center_z ) );
	
	if ( faces & CubeFace::Type::Back )
		this->faces.push_back( CubeFace( CubeFace::Type::Back, center_x, center_y, center_z ) );
	
	if ( faces & CubeFace::Type::Left )
		this->faces.push_back( CubeFace( CubeFace::Type::Left, center_x, center_y, center_z ) );

	if ( faces & CubeFace::Type::Right )
		this->faces.push_back( CubeFace( CubeFace::Type::Right, center_x, center_y, center_z ) );
	
	if ( faces & CubeFace::Type::Top )
		this->faces.push_back( CubeFace( CubeFace::Type::Top, center_x, center_y, center_z ) );
	
	if ( faces & CubeFace::Type::Bottom )
		this->faces.push_back( CubeFace( CubeFace::Type::Bottom, center_x, center_y, center_z ) );
}

Cube::Cube ( const Cube& cube )
: color( cube.color ), faces( cube.faces )
{}

#endif // CUBE_CPP