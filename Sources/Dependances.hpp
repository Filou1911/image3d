#ifndef DEPENDANCES_HPP
#define DEPENDANCES_HPP

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>

#include <QtWidgets>
#include <QtOpenGL>
#include <QGLWidget>
#include <QOpenGLFunctions>
#include <QDebug>

#ifdef __APPLE__
	#include <gl.h>
	#include <glu.h>
#else
	#include <GL/gl.h>
	#include <GL/glu.h>
#endif

using namespace std;
using UChar = unsigned char;
using UInt  = unsigned int;

#endif // DEPENDANCES_HPP //
