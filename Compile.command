#!/bin/sh

if [ $# == '0' ] ; then
	echo "USAGE   : ./Compile.command [target], type \"./Compile -help\" for more informations"

elif [ $1 == "-help" ] ; then
	echo "USAGE   : \"./Compile.command [target]\". Available targets are :"
	echo "    -cmake     : run cmake"
	echo "    -make      : run make"
	echo "    -intall    : run both cmake and make"
	echo "    -clean     : delete compiled files"
	echo "    -uninstall : delete all but sources"

elif [ $1 == "-cmake" ] ; then
	mkdir CMake
	cd Cmake 
	cmake ..
	cd ..

elif [ $1 == "-make" ] ; then
	mkdir CMake
	cd CMake
	make
	cd ..

elif [ $1 == "-install" ] ; then
	mkdir CMake
	cd CMake
	cmake ..
	make
	cd ..

elif [ $1 == "-clean" ] ; then
	cd CMake
	make clean
	cd ..

elif [ $1 == "-uninstall" ] ; then
	rm -dRf Build CMakeFiles CMake
	clear

fi